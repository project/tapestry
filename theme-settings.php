<?php

/**
 * @file
 * tapestry theme-settings.php
 */
 
function tapestry_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {

  $form['page'] = array(
    '#type' => 'details',
    '#title' => t('Page settings'),
    '#open' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['font'] = array(
    '#type' => 'details',
    '#title' => t('Font settings'),
    '#open' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['misc'] = array(
    '#type' => 'details',
    '#title' => t('Misc settings'),
    '#open' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['css'] = array(
    '#type' => 'details',
    '#title' => t('Custom Css'),
    '#open' => FALSE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['page']['tapestry_style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#description' => t('Choose your favorite color.'),
    '#default_value' => theme_get_setting('tapestry_style', $theme = NULL),
    '#options' => array(
      'allstar' => t('All Star'),
      'bluecollar' => t('Bluecollar'),
      'bogart' => t('Bogart'),
      'bizcasual' => t('Business Casual'),
      'cactusbloom' => t('Cactus Bloom'),
      'dolores' => t('Dolores'),
      'dustypetrol' => t('Dusty Petrol'),
      'firenze' => t('Firenze'),
      'fusion' => t('Fusion'),
      'gerberdaisy' => t('Gerber Daisy'),
      'haarlemmod' => t('Haarlem Modern'),
      'kalamata' => t('Kalamata Cream'),
      'kobenhavn' => t('Kobenhavn Classic'),
      'antoinette' => t('Marie Antoinette'),
      'modhome' => t('Modern Home'),
      'modoffice' => t('Modern Office'),
      'orientexpress' => t('Orient Express'),
      'woodworks' => t('Scandinavian Woodworks'),
      'techoffice' => t('Tech Office'),
      'watermelon' => t('Watermelon'),
    ),
  );

  $form['page']['tapestry_fixedwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Width Size'),
    '#description' => t('Set the width. You  can set the width only in Pixel.'),
    '#default_value' => theme_get_setting('tapestry_fixedwidth', $theme = NULL),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['page']['tapestry_outsidebar'] = array(
    '#type' => 'select',
    '#title' => t('Outside Sidebar Location'),
    '#description' => t('Sets the Outside Sidebar position on left or right.'),
    '#default_value' => theme_get_setting('tapestry_outsidebar', $theme = NULL),
    '#options' => array(
      'left' => t('Outside Sidebar on Left'),
      'right' => t('Outside Sidebar on Right'),
    ),
  );

  $form['page']['tapestry_outsidebarwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Outside Sidebar Width'),
    '#description' => t('Set the width in pixel.'),
    '#default_value' => theme_get_setting('tapestry_outsidebarwidth', $theme = NULL),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['page']['tapestry_sidebarmode'] = array(
    '#type' => 'select',
    '#title' => t('Inside Sidebar Mode'),
    '#description' => t('Sets the Sidebar First and Sidebar Second positions.'),
    '#default_value' => theme_get_setting('tapestry_sidebarmode', $theme = NULL),
    '#options' => array(
      'center' => t('Content Between Inside Sidebars'),
      'left' => t('Inside Sidebars on Left'),
      'right' => t('Inside Sidebars on Right'),
    ),
  );

  $form['page']['tapestry_leftsidebarwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Left Sidebar Width'),
    '#description' => t('Set the width in Pixel.'),
    '#default_value' => theme_get_setting('tapestry_leftsidebarwidth', $theme = NULL),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['page']['tapestry_rightsidebarwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Right Sidebar Width'),
    '#description' => t('Set the width in Pixel.'),
    '#default_value' => theme_get_setting('tapestry_rightsidebarwidth', $theme = NULL),
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['font']['tapestry_fontfamily'] = array(
    '#type' => 'select',
    '#title' => t('Font Family'),
    '#description' => t('Choose your favorite Fonts'),
    '#default_value' => theme_get_setting('tapestry_fontfamily', $theme = NULL),
    '#options' => array(
       'Arial, Verdana, sans-serif' => t('Arial, Verdana, sans-serif'),
       '"Arial Narrow", Arial, Helvetica, sans-serif' => t('"Arial Narrow", Arial, Helvetica, sans-serif'),
       '"Times New Roman", Times, serif' => t('"Times New Roman", Times, serif'),
       '"Lucida Sans", Verdana, Arial, sans-serif' => t('"Lucida Sans", Verdana, Arial, sans-serif'),
       '"Lucida Grande", Verdana, sans-serif' => t('"Lucida Grande", Verdana, sans-serif'),
       'Tahoma, Verdana, Arial, Helvetica, sans-serif' => t('Tahoma, Verdana, Arial, Helvetica, sans-serif'),
       'Georgia, "Times New Roman", Times, serif' => t('Georgia, "Times New Roman", Times, serif'),
       'Custom' => t('Custom (specify below)'),
    ),
  );

  $form['font']['tapestry_customfont'] = array(
    '#type' => 'textfield',
    '#title' => t('Custom Font-Family Setting'),
    '#description' => t('type your fonts separated by ,<br />eg. <b>"Lucida Grande", Verdana, sans-serif</b>'),
    '#default_value' => theme_get_setting('tapestry_customfont', $theme = NULL),
    '#size' => 40,
    '#maxlength' => 75,
  );
/*
  $form['css']['tapestry_custom_css_file'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include Custom Stylesheet'),
    '#description' => t('add the css code in the file custom.css is locadet in folder css'),
    '#default_value' => theme_get_setting('tapestry_custom_css_file'),
  );
*/
  $form['css']['tapestry_custom_css_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the CSS in the box below'),
    //'#description' => t('Use the CSS in the box below'),
    '#default_value' => theme_get_setting('tapestry_custom_css_text', $theme = NULL),
  );
/*
  $form['css']['tapestry_localcontentfile'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to Custom Stylesheet'),
    '#description' => t('type the location of your custom css without leading slash<br />eg. <b>sites/all/themes/newsflash/css/icons.css</b> <span style="color:red;">notwork</span>'),
    '#default_value' => theme_get_setting('tapestry_localcontentfile'),
    '#size' => 40,
    '#maxlength' => 75,
  );
*/
  $form['css']['tapestry_custom_css'] =
    array(
    '#type' => 'textarea',
    '#title' => t('Custom style area'),
    '#description' => t('You can write your own css code here'),
    '#default_value' => theme_get_setting('tapestry_custom_css', $theme = NULL),
  );

  $form['page']['tapestry_suckerfishalign'] = array(
    '#type' => 'select',
    '#title' => t('Suckerfish Menu Alignment'),
    '#description' => t('Sets the Suckerfish position.'),
    '#default_value' => theme_get_setting('tapestry_suckerfishalign', $theme = NULL),
    '#options' => array(
      'right' => t('Right'),
      'center' => t('Centered'),
      'left' => t('Left'),
    ),
  );

  $form['misc']['tapestry_themelogo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Themed Logo'),
    '#default_value' => theme_get_setting('tapestry_themelogo', $theme = NULL),
  );
/*
  $form['misc']['tapestry_breadcrumb'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Breadcrumbs'),
    '#default_value' => theme_get_setting('tapestry_breadcrumb'),
  );
*/
  $form['misc']['tapestry_pickstyle'] = array
  (
    '#type' => 'checkbox',
    '#title' => t('Enable StylePicker JavaScript'),
    '#description' => t('If enabled then you can use stylepicker see README.txt.'),
    '#default_value' => theme_get_setting('tapestry_pickstyle', $theme = NULL),
  );

  $form['misc']['tapestry_useicons'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Icons'),
    '#default_value' => theme_get_setting('tapestry_useicons', $theme = NULL),
  );

  $form['misc']['tapestry_footerlogo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Roople Footer Logo'),
    '#description' => t('if unchecked then roople logo in the footer will disapear<br>so you don\'t need touch the code'),
    '#default_value' => theme_get_setting('tapestry_footerlogo', $theme = NULL),
  );

//  return $form;
}
